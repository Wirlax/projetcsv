import csv
import argparse
import os
import sys


def lectureFichierCSV(fichierentrant, delimiter=';'):
    lignes = []
    with open(fichierentrant, 'r') as csvin:
        csvreader = csv.DictReader(csvin, delimiter=delimiter)
        for ligne in csvreader:
            lignes.append(ligne)
    return lignes

def ecritureFichierCSV(fichiersortant, ligne, delimiter=args.caractere):
    with open(fichiersortant, 'w+') as csvout:
        csvwriter = csv.DictWriter(csvout, delimiter=delimiter)
        csvwriter.writerow(conversion_liste(ligne))

def conversion_liste(liste_ProSIV):
    dico = {"adresse_titulaire":"address", "nom":"name", "prenom":"firstname", "immatriculation":"immat", "date_immatriculation":"date_immat",
        "vin":"vin", "marque":"marque", "denomination_commerciale":"denomination", "couleur":"couleur", "carrosserie":"carrosserie",
        "categorie":"categorie", "cylindree":"cylindree", "energie":"energy", "places":"places", "poids":"poids", "puissance":"puissance"}

    liste_FM = {}
    for key, value in dico.items() :
        liste_FM[key] = liste_ProSIV[value]

    type_variante_version = liste_ProSIV["type_variante_version"].split(';')
    liste_FM["type"]=type_variante_version[0]
    liste_FM["variante"]=type_variante_version[1]
    liste_FM["version"]=type_variante_version[2]

    return liste_FM

parser = argparse.ArgumentParser(description='Transforme le fichier csv en un autre fichier csv')
parser.add_argument('fichierentrant', help='le chemin du fichier à convertir')
parser.add_argument('fichiersortant', help='chemin du fichier dans lequel écrire la conversion')
parser.add_argument('caractere',help='caractère qui servira de délimiteur')
args = parser.parse_args()

if not os.path.isfile(args.fichierentrant):
    print("ERROR 101: %s Ce n'est pas un fichier valide !" % args.fichierentrant)
    sys.exit(101)
    
lignes = lectureFichierCSV(args.fichierentrant, delimiter=';')

for ligne in lignes:
    ecritureFichierCSV(args.fichiersortant,ligne, delimiter=args.caractere)
print('%s écriture réussit !' % args.fichiersortant)